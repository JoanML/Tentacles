I got busy with other stuff and left this project unatended for a while, and in the meantime, someone came up with a similar idea, so please check it out instead!

https://github.com/mjakeman/extension-manager

# Tentacles

Manage your GNOME extensions and find new ones.

With this program, you can enable, disable and uninstall your extensions,
as well as browse for new ones and install them.

<div align="center">
![Manage page](data/resources/screenshots/screenshot1.png "Manage page")
</div>

<div align="center">
![Discover page](data/resources/screenshots/screenshot2.png "Discover page")
</div>

Made with [gtk-rust-template](https://gitlab.gnome.org/bilelmoussaoui/gtk-rust-template)
with stolen ideas and patterns from other GNOME apps.

## Building and running

This is pre-realease software.
I recommend using GNOME Builder to build and run the program.
Alternatively, you may install GNOME's SDK and platform (master branch) on flatpak
and use flatpak-builder.
