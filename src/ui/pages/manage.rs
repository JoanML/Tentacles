use adw::subclass::prelude::*;
use gtk::gio;
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use std::cell::RefCell;
use std::rc::Rc;
use zbus::blocking::Connection;

use crate::api::shell::{ExtensionInfo, ExtensionSource, SessionManagerProxyBlocking};
use crate::ui::widgets;
use crate::EXTMANAGER;

mod imp {
    use super::*;

    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/joanml/Tentacles/ui/manage_page.ui")]
    pub struct ManagePage {
        #[template_child]
        pub scrolledwindow: TemplateChild<gtk::ScrolledWindow>,
        #[template_child]
        pub listbox_manual: TemplateChild<gtk::ListBox>,
        #[template_child]
        pub listbox_builtin: TemplateChild<gtk::ListBox>,
        #[template_child]
        pub updates_bar: TemplateChild<gtk::ActionBar>,
        #[template_child]
        pub updates_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub updates_button: TemplateChild<gtk::Button>,

        pub uuids_manual: Rc<RefCell<Vec<String>>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ManagePage {
        const NAME: &'static str = "ManagePage";
        type Type = super::ManagePage;
        type ParentType = adw::Bin;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        // You must call `Widget`'s `init_template()` within `instance_init()`.
        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ManagePage {}

    impl WidgetImpl for ManagePage {}

    impl BinImpl for ManagePage {}
}

glib::wrapper! {
    pub struct ManagePage(ObjectSubclass<imp::ManagePage>)
        @extends gtk::Widget, adw::Bin;
}

impl ManagePage {
    pub fn init(&self, extensions_enabled_action: &gio::SimpleAction) {
        let imp = imp::ManagePage::from_instance(self);

        // This action is passed to the widgets so that it is activated when the user uninstalls
        // an extension. Then, the page will look for the extension that was uninstalled by UUID.
        // All UUIDs are listed, and sorted, in uuids_manual: Rc<RefCell<Vec<String>>>
        let uninstall_action =
            gio::SimpleAction::new("uninstalled", Some(&glib::VariantType::new("s").unwrap()));

        let extension_list: Vec<ExtensionInfo> = EXTMANAGER
            .list_extensions()
            .unwrap()
            .values()
            .cloned()
            .collect();

        let mut needs_update: u32 = 0;

        for ext in extension_list {
            let source = ext.source;
            let uuid = ext.uuid.clone();
            let has_update = ext.has_update.clone();
            let row =
                widgets::ExtensionRowManage::new(ext, &uninstall_action, extensions_enabled_action);
            match ExtensionSource::from(source) {
                ExtensionSource::Builtin => {
                    imp.listbox_builtin.append(&row);
                }
                ExtensionSource::Manual => {
                    if has_update {
                        needs_update += 1;
                    };
                    let uuid = uuid.clone();
                    imp.uuids_manual.borrow_mut().push(uuid.to_string());
                    imp.listbox_manual.append(&row);
                }
            };
        }

        if needs_update > 0 {
            imp.updates_bar.set_revealed(true);
            imp.updates_label
                .set_label(&format!("{} Updates Available", needs_update));
        }

        imp.updates_button.connect_clicked(move |_| {
            let connection = Connection::session().expect("Could not connect to D-Bus");
            let proxy =
                SessionManagerProxyBlocking::new(&connection).expect("Could not setup proxy");
            proxy.logout(0u32).unwrap();
        });

        uninstall_action.connect_activate(glib::clone!(@weak imp.uuids_manual as uuids_manual,
        @strong imp.listbox_manual as listbox_manual => move |_, params| {
            let uuid = params.unwrap().get::<String>().unwrap();
            let position = uuids_manual.borrow().iter().position(|x| x == &uuid).unwrap();
            let row = listbox_manual.row_at_index(position as i32).unwrap();
            uuids_manual.borrow_mut().remove(position);
            listbox_manual.remove(&row);
        }));
    }

    pub fn add_row_from_uuid(&self, uuid: &str, extensions_enabled_action: &gio::SimpleAction) {
        let imp = imp::ManagePage::from_instance(self);
        let ext = EXTMANAGER.get_extension_info(&uuid).unwrap();
        let uninstall_action =
            gio::SimpleAction::new("uninstalled", Some(&glib::VariantType::new("s").unwrap()));
        let row =
            widgets::ExtensionRowManage::new(ext, &uninstall_action, extensions_enabled_action);
        imp.uuids_manual.borrow_mut().push(uuid.to_string());
        imp.listbox_manual.append(&row);
        uninstall_action.connect_activate(glib::clone!(@weak imp.uuids_manual as uuids_manual,
        @strong imp.listbox_manual as listbox_manual => move |_, params| {
            let uuid = params.unwrap().get::<String>().unwrap();
            let position = uuids_manual.borrow().iter().position(|x| x == &uuid).unwrap();
            let row = listbox_manual.row_at_index(position as i32).unwrap();
            uuids_manual.borrow_mut().remove(position);
            listbox_manual.remove(&row);
        }));
    }
}
