pub mod manage;
pub mod search;

pub use manage::ManagePage;
pub use search::SearchPage;
