use adw::subclass::prelude::*;
use gtk::gio;
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use std::thread;

use crate::api::extensions;
use crate::ui::widgets;
use crate::EXTMANAGER;

mod imp {
    use super::*;

    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/joanml/Tentacles/ui/search_page.ui")]
    pub struct SearchPage {
        #[template_child]
        pub searchbar: TemplateChild<gtk::SearchBar>,
        #[template_child]
        pub searchentry: TemplateChild<gtk::SearchEntry>,
        #[template_child]
        pub stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub scrolledwindow: TemplateChild<gtk::ScrolledWindow>,
        #[template_child]
        pub label: TemplateChild<gtk::Label>,
        #[template_child]
        pub spinner: TemplateChild<gtk::Spinner>,
        #[template_child]
        pub listbox: TemplateChild<gtk::ListBox>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SearchPage {
        const NAME: &'static str = "SearchPage";
        type Type = super::SearchPage;
        type ParentType = adw::Bin;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        // You must call `Widget`'s `init_template()` within `instance_init()`.
        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SearchPage {}

    impl WidgetImpl for SearchPage {}

    impl BinImpl for SearchPage {}
}

glib::wrapper! {
    pub struct SearchPage(ObjectSubclass<imp::SearchPage>)
        @extends gtk::Widget, adw::Bin;
}

impl SearchPage {
    pub fn init(&self, action: &gio::SimpleAction) {
        let imp = imp::SearchPage::from_instance(self);
        imp.searchbar.set_search_mode(true);
        imp.stack.set_visible_child(&imp.label.get());

        self.setup_signals(action);
    }

    fn setup_signals(&self, action: &gio::SimpleAction) {
        let (sender, receiver) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        let shell_version = EXTMANAGER.shell_version().unwrap();
        let imp = imp::SearchPage::from_instance(self);
        imp.searchentry.connect_search_changed(
            gtk::glib::clone!(@strong self as this, => move |entry| {
                let query = entry.chars(0, -1).as_str().to_owned();
                if query.is_empty() {
                    return;
                }

                let imp = imp::SearchPage::from_instance(&this);

                let mut child = imp.listbox.first_child();
                while let Some(c) = child {
                    imp.listbox.remove(&c);
                    child = imp.listbox.first_child();
                }

                imp.stack.set_visible_child(&imp.spinner.get());

                thread::spawn(glib::clone!(@strong sender, @strong shell_version => move || {
                    let client = extensions::create_client();
                    let query_result = extensions::search_extensions(&client, &query, &shell_version);
                    sender.send(query_result).expect("Could not send through channel");
                }));

            }),
        );
        receiver.attach(
            None,
            glib::clone!(@strong self as this, @strong action => @default-return Continue(false),
                move |query_result| {
                    let installed_extensions = EXTMANAGER.list_extensions();
                    let uuid_list = installed_extensions.unwrap().keys().cloned().collect();
                    let imp = imp::SearchPage::from_instance(&this);
                    match query_result {
                        Ok(extensions_list) => for ext in extensions_list {
                            let extension_row = widgets::ExtensionRowDiscover::new(ext, &uuid_list, &action);
                            imp.listbox.append(&extension_row);
                            },
                        Err(_) => println!("error"), // TODO: send notification if search fails
                    };
                    imp.stack.set_visible_child(&imp.scrolledwindow.get());
                    Continue(true)
                }
            ),
        );
    }
}
