use adw;
use adw::subclass::prelude::*;
use gdk;
use gdk::prelude::*;
use gtk::gio;
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use once_cell::unsync::OnceCell;

use crate::api::shell::{ExtensionInfo, ExtensionSource, ExtensionStatus};
use crate::EXTMANAGER;

mod imp {
    use super::*;

    use gtk::CompositeTemplate;

    #[derive(Default, Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/joanml/Tentacles/ui/extension_row_manage.ui")]
    pub struct ExtensionRowManage {
        #[template_child]
        pub image: TemplateChild<gtk::Image>,
        #[template_child]
        pub switch: TemplateChild<gtk::Switch>,
        #[template_child]
        pub prefs_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub website_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub uninstall_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub description: TemplateChild<gtk::Label>,
        #[template_child]
        pub version: TemplateChild<gtk::Label>,

        pub extension: OnceCell<ExtensionInfo>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ExtensionRowManage {
        const NAME: &'static str = "ExtensionRowManage";
        type Type = super::ExtensionRowManage;
        type ParentType = adw::ExpanderRow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        // You must call `Widget`'s `init_template()` within `instance_init()`.
        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ExtensionRowManage {}

    impl WidgetImpl for ExtensionRowManage {}

    impl ExpanderRowImpl for ExtensionRowManage {}

    impl PreferencesRowImpl for ExtensionRowManage {}

    impl ListBoxRowImpl for ExtensionRowManage {}
}

glib::wrapper! {
    pub struct ExtensionRowManage(ObjectSubclass<imp::ExtensionRowManage>)
        @extends gtk::Widget, adw::ExpanderRow, gtk::Button;
}

impl ExtensionRowManage {
    pub fn new(
        ext: ExtensionInfo,
        uninstall_action: &gio::SimpleAction,
        extensions_enabled_action: &gio::SimpleAction,
    ) -> Self {
        let row = glib::Object::new::<Self>(&[]).unwrap();
        row.set_property("title", &ext.name).unwrap();

        row.setup_properties(ext);
        row.setup_signals(uninstall_action, extensions_enabled_action);

        row
    }

    fn setup_properties(&self, ext: ExtensionInfo) {
        let imp = imp::ExtensionRowManage::from_instance(self);

        imp.description.set_label(&ext.description);

        if ext.has_update {
            imp.image.set_visible(true);
        }

        if ext.version.is_some() {
            imp.version.set_label(&ext.version.unwrap().to_string());
        }

        if !ext.has_prefs {
            imp.prefs_button.set_visible(false);
        }

        match ExtensionSource::from(ext.source) {
            ExtensionSource::Builtin => {
                imp.uninstall_button.set_visible(false);
            }
            ExtensionSource::Manual => (),
        };

        if EXTMANAGER.user_extensions_enabled().unwrap() {
            imp.switch.set_sensitive(true);
        } else {
            imp.switch.set_sensitive(false);
        };

        match ExtensionStatus::from(ext.state) {
            ExtensionStatus::Enabled => imp.switch.set_active(true),
            ExtensionStatus::Initialized => imp.switch.set_active(false),
            ExtensionStatus::Disabled => imp.switch.set_active(false),
            _ => imp.switch.set_sensitive(false),
        };

        imp.extension.set(ext).unwrap();
    }

    fn setup_signals(
        &self,
        uninstall_action: &gio::SimpleAction,
        extensions_enabled_action: &gio::SimpleAction,
    ) {
        let context = gdk::Display::default().unwrap().app_launch_context();
        let imp = imp::ExtensionRowManage::from_instance(self);

        imp.switch.connect_state_set(
            glib::clone!(@strong imp.extension as extension, @strong imp.switch as switch => move |switch, new_state| {
                let uuid = &extension.get().unwrap().uuid;
                if new_state {
                    match EXTMANAGER.enable_extension(uuid).unwrap() {
                        true => switch.set_state(true),
                        false => (),
                    };
                } else {
                    match EXTMANAGER.disable_extension(&uuid).unwrap() {
                        true => switch.set_state(false),
                        false => (),
                    };
                }
                gtk::Inhibit(true)
            }),
        );

        imp.prefs_button.connect_clicked(
            glib::clone!(@strong imp.extension as extension => move |_| {
                let uuid = &extension.get().unwrap().uuid;
                EXTMANAGER.launch_extension_prefs(uuid).unwrap();
            }),
        );

        imp.website_button.connect_clicked(
            glib::clone!(@strong imp.extension as extension, @strong context => move |_| {
                let url = &extension.get().unwrap().url;
                gio::AppInfo::launch_default_for_uri(url, Some(&context)).unwrap();
            }),
        );

        imp.uninstall_button.connect_clicked(
            glib::clone!(@strong imp.extension as extension, @strong imp.prefs_button as prefs_button, @strong uninstall_action as action => move |_| {
                let uuid = &extension.get().unwrap().uuid;
                let out = EXTMANAGER.uninstall_extension(uuid).unwrap();
                match out {
                    true => {
                        prefs_button.set_sensitive(false);
                    }
                    false => (), // TODO: send notification
                }
                action.activate(Some(&uuid.to_variant()));
            }),
        );

        extensions_enabled_action.connect_activate(
            glib::clone!(@strong imp.switch as switch, @strong imp.extension as extension => move |_, new_switch_state| {
                let uuid = &extension.get().unwrap().uuid;
                let extension_state = ExtensionStatus::from(EXTMANAGER.get_extension_info(&uuid).unwrap().state);
                let new_switch_state = new_switch_state.unwrap().get::<bool>().unwrap();
                if new_switch_state {
                    switch.set_sensitive(true);
                    match extension_state {
                        ExtensionStatus::Initialized => switch.set_active(false),
                        ExtensionStatus::Disabled => {
                            match EXTMANAGER.enable_extension(&uuid).unwrap() {
                                true => switch.set_active(true),
                                false => (), // TODO: send notification
                            };
                        },
                        _ => switch.set_sensitive(false),
                    };
                } else {
                    switch.set_active(false);
                    switch.set_sensitive(false);
                }
            }),
        );
    }
}
