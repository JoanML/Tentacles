use adw;
use adw::subclass::prelude::*;
use gdk;
use gdk::prelude::*;
use gtk::gio;
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use once_cell::unsync::OnceCell;
use std::str::FromStr;

use crate::api::extensions;
use crate::api::shell::InstallOutcome;
use crate::EXTMANAGER;

mod imp {
    use super::*;

    use gtk::CompositeTemplate;

    #[derive(Default, Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/joanml/Tentacles/ui/extension_row_discover.ui")]
    pub struct ExtensionRowDiscover {
        #[template_child]
        pub button: TemplateChild<gtk::Button>,
        #[template_child]
        pub website_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub description: TemplateChild<gtk::Label>,
        #[template_child]
        pub author: TemplateChild<gtk::Label>,

        pub extension: OnceCell<extensions::Extension>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ExtensionRowDiscover {
        const NAME: &'static str = "ExtensionRowDiscover";
        type Type = super::ExtensionRowDiscover;
        type ParentType = adw::ExpanderRow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        // You must call `Widget`'s `init_template()` within `instance_init()`.
        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ExtensionRowDiscover {}

    impl WidgetImpl for ExtensionRowDiscover {}

    impl ExpanderRowImpl for ExtensionRowDiscover {}

    impl PreferencesRowImpl for ExtensionRowDiscover {}

    impl ListBoxRowImpl for ExtensionRowDiscover {}
}

glib::wrapper! {
    pub struct ExtensionRowDiscover(ObjectSubclass<imp::ExtensionRowDiscover>)
        @extends gtk::Widget, adw::ExpanderRow, gtk::Button;
}

impl ExtensionRowDiscover {
    pub fn new(
        ext: extensions::Extension,
        uuid_list: &Vec<String>,
        action: &gio::SimpleAction,
    ) -> Self {
        let row = glib::Object::new::<Self>(&[]).unwrap();
        let context = gdk::Display::default().unwrap().app_launch_context();
        let imp = imp::ExtensionRowDiscover::from_instance(&row);

        imp.description.set_label(&ext.description);
        imp.author.set_label(&ext.creator);
        row.set_property("title", glib::markup_escape_text(&ext.name))
            .unwrap();

        imp.extension.set(ext).unwrap();
        let button = imp.button.get();

        button.set_label("Install");
        if uuid_list.contains(&imp.extension.get().unwrap().uuid) {
            button.set_sensitive(false);
        } else {
            button.style_context().add_class("suggested-action");
        }

        button.connect_clicked(
            glib::clone!(@strong imp.extension as extension, @strong action => move |button| {
                let uuid = &extension.get().unwrap().uuid;
                let out = EXTMANAGER.install_remote_extension(uuid);
                match out {
                    Ok(result) => {
                        match InstallOutcome::from_str(&result).unwrap() {
                            InstallOutcome::Successful => {
                                button.set_sensitive(false);
                                button.style_context().remove_class("suggested-action");
                                action.activate(Some(&uuid.to_variant())); // Some(uuid)
                            },
                            InstallOutcome::Cancelled => (),
                        };
                    },
                    Err(_) => (), // TODO: send notification
                }
            }),
        );

        imp.website_button
            .connect_clicked(glib::clone!(@strong imp.extension as extension, @strong context => move |_| {
                let full_url = "https://extensions.gnome.org".to_owned() + &extension.get().unwrap().url;
                gio::AppInfo::launch_default_for_uri(&full_url, Some(&context)).unwrap();
            }));

        row
    }
}
