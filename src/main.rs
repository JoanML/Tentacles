mod api;
mod application;
#[rustfmt::skip]
mod config;
mod ui;
mod window;

use crate::api::shell::ExtensionsProxyBlocking;
use application::Application;
use config::{GETTEXT_PACKAGE, LOCALEDIR, RESOURCES_FILE};
use gettextrs::{gettext, LocaleCategory};
use gtk::{gio, glib};
use once_cell::sync::Lazy;
use zbus::blocking::Connection;

pub static EXTMANAGER: Lazy<ExtensionsProxyBlocking> = Lazy::new(|| {
    let connection = Connection::session().expect("Could not connect to D-Bus");
    ExtensionsProxyBlocking::new(&connection).expect("Could not setup proxy")
});

fn main() {
    // Initialize logger
    pretty_env_logger::init();

    // Prepare i18n
    gettextrs::setlocale(LocaleCategory::LcAll, "");
    gettextrs::bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR).expect("Unable to bind the text domain");
    gettextrs::textdomain(GETTEXT_PACKAGE).expect("Unable to switch to the text domain");

    glib::set_application_name(&gettext("Tentacles"));

    gtk::init().expect("Unable to start GTK4");

    let res = gio::Resource::load(RESOURCES_FILE).expect("Could not load gresource file");
    gio::resources_register(&res);

    let app = Application::new();
    app.run();
}
