use reqwest::blocking::Client;
use serde::Deserialize;

#[derive(Clone, Deserialize, Debug)]
pub struct Extension {
    pub uuid: String,
    pub name: String,
    pub creator: String,
    pub description: String,
    #[serde(rename = "link")]
    pub url: String,
}

#[derive(Clone, Deserialize)]
struct ExtensionsQuery {
    pub extensions: Vec<Extension>,
    pub numpages: u32,
}

pub fn create_client() -> Client {
    Client::new()
}

fn build_url(page: u32, query: &str, ver: &str) -> String {
    "https://extensions.gnome.org/extension-query/?page=".to_owned()
        + &page.to_string()
        + "&search="
        + query
        + "&shell_version="
        + ver
}

pub fn search_extensions(
    client: &Client,
    query: &str,
    ver: &str,
) -> Result<Vec<Extension>, reqwest::Error> {
    let url = build_url(1, query, ver);
    let resp: ExtensionsQuery = client.get(url).send()?.json()?;
    let num_pages = resp.numpages;
    let mut extensions_list = resp.extensions;
    for i in 2..=num_pages {
        let url = build_url(i, query, ver);
        let mut resp: ExtensionsQuery = client.get(url).send()?.json()?;
        extensions_list.append(&mut resp.extensions);
    }
    Ok(extensions_list)
}
