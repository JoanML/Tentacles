use serde::Deserialize;
use std::collections::HashMap;
use std::fmt;
use std::str::FromStr;
use zbus::dbus_proxy;
use zvariant::derive::*;

#[derive(Clone, Debug, Deserialize, Type)]
pub enum ExtensionSource {
    Builtin,
    Manual,
}

impl From<f64> for ExtensionSource {
    fn from(v: f64) -> ExtensionSource {
        match v.round() as isize {
            1 => ExtensionSource::Builtin,
            2 => ExtensionSource::Manual,
            _ => panic!("Unexpected extension status. Please report this error."),
        }
    }
}

#[derive(Clone, Debug, Deserialize, Type)]
pub enum ExtensionStatus {
    Enabled,
    Disabled,
    Error,
    OutOfDate,
    Downloading,
    Initialized,
    Uninstalled,
}

impl From<f64> for ExtensionStatus {
    fn from(v: f64) -> ExtensionStatus {
        match v.round() as isize {
            1 => ExtensionStatus::Enabled,
            2 => ExtensionStatus::Disabled,
            3 => ExtensionStatus::Error,
            4 => ExtensionStatus::OutOfDate,
            5 => ExtensionStatus::Downloading,
            6 => ExtensionStatus::Initialized,
            99 => ExtensionStatus::Uninstalled,
            _ => panic!("Unexpected extension status. Please report this error."),
        }
    }
}

#[derive(Clone, Debug, DeserializeDict, TypeDict)]
pub struct ExtensionInfo {
    pub uuid: String,
    pub name: String,
    pub description: String,
    pub url: String,
    #[zvariant(rename = "type")]
    pub source: f64,
    pub state: f64,
    #[zvariant(rename = "hasPrefs")]
    pub has_prefs: bool,
    #[zvariant(rename = "hasUpdate")]
    pub has_update: bool,
    pub version: Option<f64>,
}

pub struct ParseError;

impl fmt::Debug for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Error parsing string.")
    }
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Error parsing string.")
    }
}

#[derive(Debug)]
pub enum InstallOutcome {
    Successful,
    Cancelled,
}

impl FromStr for InstallOutcome {
    type Err = ParseError;

    fn from_str(v: &str) -> Result<InstallOutcome, ParseError> {
        match v {
            "successful" => Ok(InstallOutcome::Successful),
            "cancelled" => Ok(InstallOutcome::Cancelled),
            _ => Err(ParseError),
        }
    }
}

#[dbus_proxy(
    default_service = "org.gnome.Shell.Extensions",
    interface = "org.gnome.Shell.Extensions",
    default_path = "/org/gnome/Shell/Extensions"
)]
pub trait Extensions {
    #[dbus_proxy(property)]
    fn shell_version(&self) -> zbus::Result<String>;
    #[dbus_proxy(property)]
    fn user_extensions_enabled(&self) -> zbus::Result<bool>;
    #[dbus_proxy(property)]
    fn set_user_extensions_enabled(&self, v: bool) -> zbus::Result<()>;

    fn get_extension_info(&self, uuid: &str) -> zbus::Result<ExtensionInfo>;
    fn list_extensions(&self) -> zbus::Result<HashMap<String, ExtensionInfo>>;
    fn launch_extension_prefs(&self, uuid: &str) -> zbus::Result<()>;
    fn enable_extension(&self, uuid: &str) -> zbus::Result<bool>;
    fn disable_extension(&self, uuid: &str) -> zbus::Result<bool>;
    fn install_remote_extension(&self, uuid: &str) -> zbus::Result<String>;
    fn uninstall_extension(&self, uuid: &str) -> zbus::Result<bool>;
}

#[dbus_proxy(
    default_service = "org.gnome.SessionManager",
    interface = "org.gnome.SessionManager",
    default_path = "/org/gnome/SessionManager"
)]
pub trait SessionManager {
    fn logout(&self, mode: u32) -> zbus::Result<()>;
}
