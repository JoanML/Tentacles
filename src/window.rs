use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib};

use crate::application::Application;
use crate::config::{APP_ID, PROFILE};
use crate::EXTMANAGER;

use crate::ui::pages::*;

mod imp {
    use super::*;

    use gtk::CompositeTemplate;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/joanml/Tentacles/ui/window.ui")]
    pub struct ApplicationWindow {
        #[template_child]
        pub manage_page: TemplateChild<manage::ManagePage>,
        #[template_child]
        pub search_page: TemplateChild<search::SearchPage>,
        #[template_child]
        pub headerbar: TemplateChild<adw::HeaderBar>,
        #[template_child]
        pub switch: TemplateChild<gtk::Switch>,
        pub settings: gio::Settings,
    }

    impl Default for ApplicationWindow {
        fn default() -> Self {
            Self {
                manage_page: TemplateChild::default(),
                search_page: TemplateChild::default(),
                headerbar: TemplateChild::default(),
                switch: TemplateChild::default(),
                settings: gio::Settings::new(APP_ID),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ApplicationWindow {
        const NAME: &'static str = "ApplicationWindow";
        type Type = super::ApplicationWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        // You must call `Widget`'s `init_template()` within `instance_init()`.
        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ApplicationWindow {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            // Devel Profile
            if PROFILE == "Devel" {
                obj.add_css_class("devel");
            }

            // Load latest window state
            obj.load_window_size();
        }
    }

    impl WidgetImpl for ApplicationWindow {}
    impl WindowImpl for ApplicationWindow {
        // Save window state on delete event
        fn close_request(&self, window: &Self::Type) -> gtk::Inhibit {
            if let Err(err) = window.save_window_size() {
                log::warn!("Failed to save window state, {}", &err);
            }

            // Pass close request on to the parent
            self.parent_close_request(window)
        }
    }

    impl ApplicationWindowImpl for ApplicationWindow {}

    impl AdwApplicationWindowImpl for ApplicationWindow {}
}

glib::wrapper! {
    pub struct ApplicationWindow(ObjectSubclass<imp::ApplicationWindow>)
        @extends gtk::Widget, gtk::Window, adw::Window, adw::ApplicationWindow,
        @implements gio::ActionMap, gio::ActionGroup;
}

impl ApplicationWindow {
    pub fn new(app: &Application) -> Self {
        let window = glib::Object::new::<Self>(&[("application", app)])
            .expect("Failed to create ApplicationWindow");

        let imp = imp::ApplicationWindow::from_instance(&window);

        // The folowing action is passed to the search page and to the searched extension widgets
        // When an action is installed, it is activated
        // Then, the manage page fetches the extension data from D-Bus and adds it to the list
        let installed_action =
            gio::SimpleAction::new("installed", Some(&glib::VariantType::new("s").unwrap()));

        // The following action is passed to the manage page and to the widgets, so that they
        // know to disable the switches when extensions are disabled, and viceversa
        let extensions_enabled_action =
            gio::SimpleAction::new("enabled", Some(&glib::VariantType::new("b").unwrap()));

        imp.search_page.init(&installed_action);
        imp.manage_page.init(&extensions_enabled_action);

        if EXTMANAGER.user_extensions_enabled().unwrap() {
            imp.switch.set_active(true);
        } else {
            imp.switch.set_active(false);
        }

        window.setup_signals(installed_action, extensions_enabled_action);

        window
    }

    fn setup_signals(
        &self,
        installed_action: gio::SimpleAction,
        extensions_enabled_action: gio::SimpleAction,
    ) {
        let imp = imp::ApplicationWindow::from_instance(self);

        imp.switch
            .connect_state_set(glib::clone!(@strong imp.switch as switch,
            @strong extensions_enabled_action as action => move |switch, new_state| {
                EXTMANAGER.set_user_extensions_enabled(new_state).unwrap();
                switch.set_state(new_state);
                action.activate(Some(&new_state.to_variant()));
                gtk::Inhibit(true)
            }));

        installed_action.connect_activate(glib::clone!(@strong imp.manage_page as page,
        @strong extensions_enabled_action as action =>  move |_, uuid| {
            page.add_row_from_uuid(&uuid.unwrap().get::<String>().unwrap(), &action);
        }));
    }

    fn save_window_size(&self) -> Result<(), glib::BoolError> {
        let imp = imp::ApplicationWindow::from_instance(self);

        let (width, height) = self.default_size();

        imp.settings.set_int("window-width", width)?;
        imp.settings.set_int("window-height", height)?;

        imp.settings
            .set_boolean("is-maximized", self.is_maximized())?;

        Ok(())
    }

    fn load_window_size(&self) {
        let imp = imp::ApplicationWindow::from_instance(self);

        let width = imp.settings.int("window-width");
        let height = imp.settings.int("window-height");
        let is_maximized = imp.settings.boolean("is-maximized");

        self.set_default_size(width, height);

        if is_maximized {
            self.maximize();
        }
    }
}
